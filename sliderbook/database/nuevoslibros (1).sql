-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2014 a las 04:07:19
-- Versión del servidor: 5.6.11
-- Versión de PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nuevoslibros`
--
CREATE DATABASE IF NOT EXISTS `nuevoslibros` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nuevoslibros`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nuevoslibros2`
--

CREATE TABLE IF NOT EXISTS `nuevoslibros2` (
  `Id` int(11) DEFAULT NULL,
  `Titulo` varchar(255) DEFAULT NULL,
  `Autor` varchar(255) DEFAULT NULL,
  `ISBN` varchar(255) DEFAULT NULL,
  `Editorial` varchar(255) DEFAULT NULL,
  `Año` int(11) DEFAULT NULL,
  `Subtitulo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nuevoslibros2`
--

INSERT INTO `nuevoslibros2` (`Id`, `Titulo`, `Autor`, `ISBN`, `Editorial`, `Año`, `Subtitulo`) VALUES
(1, 'El despertar de la Gracia.', 'Charles R. Swindoll', '9780881130188', 'Grupo Nelson', 2013, 'Creer en la gracia es una cosa. Vivirla es otra'),
(2, 'The complete book of speech Communication', 'Carol Marrs: illustrated  by Lafe Locke', '0916260879', 'MCMXCII Meriwether Publishing Ltd.', 1992, 'Workbook of ideas and activities for students of speech and eheatre'),
(3, 'The Communication skill Workbook', 'Ester A Leutenberg, John J. Liptak, Illustrated by Amy L. Brodsky, LISW.', '9781570252266', 'Whole Person Associates', 2008, 'Selt-Assessments, Exercises and Educational Handouts'),
(4, 'Teaching ESL/EFL. Listeninng and Speaking', 'Jonathan Newton', '9780415989701', 'Routledge', 2009, 'ESL & Applied Linguistics Professional Series'),
(5, 'Diccionario Medico', 'Manuel Álvarez-Uría, Pedro Riera Rovira', '8495998688', 'Madu', 2005, 'Cientifico y Divulgativo'),
(6, 'Platón, Diálogos.', 'Estudio Preliminar Francisco Larroyo', '9789700772470', 'Porrúa', 2012, 'Apología de Socrates, Critón o del deber, Eutifrón o de la santidad, Laques o del valor, Lysis o de la Amistad,Carmides o de la templanza, Ion o de la poesia, Protágoras o de los sofistas, gorgias o de la retorica, Menón o de la virtud, etc.'),
(7, 'Platon, Diálogos.', 'Estudio preliminar Francisco Larroyo', '9789700772470', 'Porrúa', 2012, 'La república o de lo justo, Fedro o del amor, Timeo o de la naturaleza, Gritias o de la Atlántida, El sofista o del ser.'),
(8, 'Conversando el Inglés', 'Prof. Jaime Garza Bores.', '9780071440066', 'McGraw Hill', 2004, 'Desarrolle fluidez en el inglés hablando. Mejore su habilidad de expresarse. Practique con docena de diálogos y ejercicios prácticos.'),
(9, 'The best test preparation for the CLEP, college-level Examination Program', 'Jacob Stratman, Ph. D.', '9780738605593', 'Green Edition', 2010, 'American Literature'),
(10, 'The oxford illustrated history of englich literature', 'Pat Rogers', '9780192854377', 'Oxford University press', 201, NULL),
(11, 'El sí de las Niñas', 'Leandro F. de Moratín', '9789706271310', 'Epoca, S.A.', 2008, NULL),
(12, 'Manual de Terapia de Juego', 'Charles E. Schaefer, Kevin J. O´Connor', '9684461', 'El Manual Moderno', 1988, 'Volumen1'),
(13, 'Manual de Terapia de Juego', 'Charles E. Schaefer, Kevin J. O´Connor', '9684267525', 'El Manual Moderno', 1997, 'Avances e innovaciones. Volumen 2'),
(14, 'Psicoterapia infantil con juego', 'Fayne Esquivel Ancona', '9786074480559', 'El Manual Moderno', 2010, 'Casos Clínicos'),
(15, 'Fisiología del esfuerzo y del deporte', 'Jack H. Wilmore, David L. Costill', '9788480199162', 'Paidotribo.', 2010, 'Sexta edición.'),
(16, 'Terminología Médica', 'Enrique Cárdenas de la Peña', '9786071505484', 'McGrawHill', 2011, 'Cuarta edición.'),
(17, 'Dulcemente Saludable', 'Lydia S. de Jiménez', '9786077730958', 'Gema Editores', 2014, 'ideas y consejos prácticos para mejorar la salud.'),
(18, 'Herramientas y soluciones para Docentes', 'Pedro Luis Llaca Gaviño, Eduardo Luevano Chavarria, Francisco J. Vazquez, Efren D.Gutierrez, Yolanda Loyo, Rodrigo Gutierrez, Jesus G., Juan J.', '968785457x', 'Euroméxico', 2011, 'Tomo1.  ¿Cómo actuar?. Hábitos. Dinámica del Aprendizaje. Apoyo de los padres.'),
(19, 'Herramientas y soluciones para Docentes', 'Pedro Luis Llaca Gaviño, Eduardo Luevano Chavarria, Francisco J. Vazquez, Efren D.Gutierrez, Yolanda Loyo, Rodrigo Gutierrez, Jesus G., Juan J.', '9709852000', 'Euroméxico', 2011, 'Tomo2. Técnicas de estudio. Valores. Imagen del maestro.'),
(20, 'Herramientas y soluciones para Docentes', 'Pedro Luis Llaca Gaviño, Eduardo Luevano Chavarria, Francisco J. Vazquez, Efren D.Gutierrez, Yolanda Loyo, Rodrigo Gutierrez, Jesus G., Juan J.', '9709852175', 'Euroméxico', 2011, 'Tomo 3. Dinámicas de grupos. El niño zurdo. Periódico Mural. Primeros Auxilios'),
(21, 'Herramientas y soluciones para Docentes', 'Pedro Luis Llaca Gaviño, Eduardo Luevano Chavarria, Francisco J. Vazquez, Efren D.Gutierrez, Yolanda Loyo, Rodrigo Gutierrez, Jesus G., Juan J.', '9709852183', 'Euroméxico', 2011, 'Tomo 4. Autoestima. Disciplica. Diccionario.'),
(22, 'Diccionario ilustrado de enfermería', 'Juventina Sánchez Guerrero', '9786071714480', 'Trillas', 2013, 'Más de 4000 terminos de uso frecuente. Atlas de Anatomía a todo color. Siglas de uso común.'),
(23, 'Cuidado del paciente en estado crítico.', 'Ángela M. Pulgarin, Sandra Osorio, Luz Varela.', '9789589076644', 'Corporación para Investigadores Biológicas.', 2012, 'Fundamentos de enfermeria.'),
(24, 'Enfermería de cuidados intensivos', 'Cynthia Lee Terry, Aurora Weaver.', '9786074482300', 'Manual moderno', 2012, NULL),
(25, 'Administración de los servicios de enfermeria', 'María de la luz Balderas Pedrero.', '9786071506856', 'McGraw Hill', 2012, NULL),
(26, 'Derecho Diplomatico', 'Pedro G. Labariega Villanueva.', '9786071710260', 'Trillas', 2012, 'Normas, usos, costumbres y cortesías.'),
(27, 'Historia Universal', 'Rodriguez Arvizu, Gómez Méndez, Ramirez Campos.', '9786070505089', 'Limusa', 2014, NULL),
(28, 'OpenGL Super Bible', 'Richard S.,Nicolas H.,Grahan S., Benjamin L.', '9780321712615', 'Person Education', 2011, 'Comprehensive Tutorial and Reference'),
(29, 'Java Cómo Programar', 'Deitel. Paul y Harvey', '9786073211505', 'Person Education', 2012, 'Novena Edición.'),
(30, 'Fisiología del esfuerzo y del deporte', 'Jack h. Wilmore, David L. Costill.', '9788480199162', 'Human Kinetics Publishers, Inc.', 2010, 'sexta e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
